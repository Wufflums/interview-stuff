<?php

/*
	Bad
 */

class Reader
{
	public function readIt (Book $book)
	{
		echo $book->read();
	}
}

class Book
{
	public function read()
	{
		
	}
	
	// Other stuff
}

/*
	Good
 */

class Reader
{
	public function readIt(Readable $readable)
	{
		echo $readable->read();
	}
}

class Book implements Readable
{
	public function read() {}
	
	// Book stuff
}

class SlightlyDifferentBook implements Readable
{
	public function read() {}
	
	// Slightly different book stuff
}
