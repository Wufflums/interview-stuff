<?php
/**
 * @see https://github.com/DealereEProcess/Phoenix/blob/master/application/libraries/SocialMedia/View/SocialMediaTwitterView.php
 */

/*

Bad

 */

interface BookInterface
{
	public function getPages();
	public function getIndex();
	public function getAppendix();
}

// Novels have pages, and also tend to have a leading index of chapters as well
class Novel implements BookInterface
{
	public function getPages() {
		// Get pages
	}
	public function getIndex() {
		// Get Index
	}
	public function getAppendix() {
		// Do nothing because we have no appendix
	}
}

// Textbooks have pages and an index like a novel, but also contain an appendix
class Textbook
{
	public function getPages() {
		// Get pages
	}
	public function getIndex() {
		// Get Index
	}
	public function getAppendix() {
		// Get Appendix
	}
}

// Magazines have pages but not an index or appendix
class Magazine
{
	public function getPages() {
		// Get pages
	}
	public function getIndex() {
		// Do nothing because we have no index
	}
	public function getAppendix() {
		// Do nothing because we have no appendix
	}
}

/*

Good

 */

interface BookInterface
{
	public function getPages ();
}

interface IndexedBookInterface
{
	public function getIndex();
}

interface AppendixedBookInterface
{
	public function getAppendix();
}

class Novel implements BookInterface, IndexedBookInterface
{
	public function getPages() {
		// Get pages
	}
	public function getIndex() {
		// Get Index
	}
}

class Textbook implements BookInterface, IndexedBookInterface, AppendixedBookInterface
{
	public function getPages() {
		// Get pages
	}
	public function getIndex() {
		// Get Index
	}
	public function getAppendix() {
		// Get Appendix
	}
}

class Magazine implements BookInterface
{
	public function getPages() {
		// Get pages
	}
}

