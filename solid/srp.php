<?php

/*
	Class with a bunch of junk happenin
 */
class Car
{
	public function getColor()
	{
		return $color;
	}
	public function getModel()
	{
		return $model;
	}
	public function getIncentives() 
	{
		// Magic incentives stuff
	}
}

/*
	Better
 */

class Car
{
	public function getColor()
	{
		return $color;
	}
	public function getModel()
	{
		return $model;
	}
}

class CarIncentives
{
	public function getIncentives (Car $car) {
		// Magic incentives stuff
	}
}
