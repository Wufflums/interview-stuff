<?php

class Vector
{
	public function setCoordinates(int $x, int $y) {}
}

class Vector3D extends Vector
{
	public function setCoordinates(int $x, int $y, int $z) {}
}

// LSP violation - Vector3D cannot be base (Vector3D cannot be used as a Vector)