<?php

/**
 * Dependency inversion plus also its a singleton
 */

class DBConnection
{
	private static $instance;
	
	private $connection;
	private $db_data;
	
	private function __construct() {
		// Set up DB connection
	}
	
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new DBConnection();
		}
		
		return self::$instance;
	}
	
	public function getConnection()
	{
		return $this->connection;
	}
}

/*
    Another one
*/

interface Parts {
    public function getBody();
    public function getEngine();
}

class CarFactory(TruckParts $parts) -> class CarFactory(Parts $parts)
{
    echo $parts->getBody().$parts->getEngine();
}

}
    
}