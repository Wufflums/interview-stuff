<?php
namespace APP\DealerGrade;

/**
 * Dealer Grade view builder.
 */
class ViewBuilder
{
	public function buildGradeAverageView(
		Data\GradeItemContent $view_data,
		bool $minify_flag = true
	) : View\GradeAverageView {
		return new View\GradeAverageView($view_data, $minify_flag);
	}
	
	public function buildGradeBreakdownView(
		Data\GradeCollection $view_data,
		bool $minify_flag = true
	) : View\GradeBreakdownView {
		return new View\GradeBreakdownView($view_data, $minify_flag);
	}
	
	public function buildGradeChartView(
		Data\GradeCollection $view_data,
		bool $minify_flag = true
	) : View\GradeChartView {
		return new View\GradeChartView($view_data, $minify_flag);
	}
	
	public function buildGradeItemsView(
		Data\GradeCollection $view_data,
		bool $minify_flag = true
	) : View\GradeItemsView {
		return new View\GradeItemsView($view_data, $minify_flag);
	}
}
