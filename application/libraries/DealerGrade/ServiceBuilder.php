<?php
namespace APP\DealerGrade;

/**
 * Service builder for Dealer Grade inventory grading system.
 */
class ServiceBuilder
{
	/**
	 * Builds the service controller which decides what service structure to go
	 * with.
	 * 
	 * @uses Controller::__construct() to instance Dealer Grade Controller
	 * 
	 * @param MysqliExtended $connection database connection
	 * 
	 * @return Controller instance of Dealer Grade controller
	 */
	public function buildController(MysqliExtended &$connection) : Controller {
		$service = $this->buildService(
			$this->buildFactory(),
			$this->buildProvider($connection)
		);
		
		$view_builder = $this->buildViewBuilder();
		
		return new Controller($service, $provider);
	}
	
	/**
	 * Builds the data processor used in processing new dealer grades.
	 * 
	 * @uses Processor::__construct() to instance Dealer Grade command line
	 *       processor
	 * 
	 * @param MysqliExtended $connection database connection
	 * @param Date           $date       date for processing
	 * @param int            $id         dealer or site ID, depending on how the
	 *                                   dealer scores should be aggregated
	 * @param string         $id_type    'dealerID' or 'siteID' type of data
	 *                                   aggrtegation
	 * 
	 * @return Processor instance of Dealer Grade processor
	 */
	public function buildProcessor(
		MysqliExtended &$connection,
		Date $date,
		int $id,
		string $id_type
	) : Processor {
		$provider = $this->buildProvider($connection);
		
		return new Processor($provider);
	}
	
	/**
	 * Builds the Dealer Grade data object factory.
	 * 
	 * @uses Factory::__construct() to instance Dealer Grade factory
	 * 
	 * @return Factory instance of Dealer Grade factory
	 */
	private function buildFactory()
	{
		return new Factory();
	}
	
	/**
	 * Builds the provider for the Dealer Grade raw data.
	 * 
	 * @uses Provider::__construct() to isntance Dealer Grade provider
	 * 
	 * @param MysqliExtended $connection database connection
	 * 
	 * @return Provider instance of Dealer Grade provider
	 */
	private function buildProvider(MysqliExtended &$connection) : Provider {
		return new Provider($connection);
	}
	
	/**
	 * Builds the Dealer Grade service.
	 * 
	 * @uses ViewBuilder::__construct() to instance Dealer Grade view builder
	 * 
	 * @return ViewBuilder instance of Dealer Grade view builder
	 */
	private function buildService(Factory $factory, Provider $provider)
	{
		return new Service($factory, $provider);
	}
	
	/**
	 * Builds the Dealer Grade view builder.
	 * 
	 * @uses ViewBuilder::__construct() to instance Dealer Grade view builder
	 * 
	 * @return ViewBuilder instance of Dealer Grade view builder
	 */
	private function buildViewBuilder()
	{
		return new ViewBuilder();
	}
}
