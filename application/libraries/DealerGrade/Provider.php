<?php
namespace APP\DealerGrade;

/**
 * Data provider for Dealer Grade service.
 */
class Provider
{
	/**
	 * @var MysqliExtended database connection
	 */
	private $connection;
	
	/**
	 * Constructor for Dealer Grade data provider.
	 * 
	 * @param MysqliExtended $connection database connection
	 * 
	 * 
	 * @throws TypeError on incorrect parameter type
	 */
	public function __construct(MysqliExtended &$connection) {
		$this->setConnection($connection);
	}
	
	/**
	 * Fetches raw site / dealer scores and returns in nice array.
	 * 
	 * @param  Date  $data_end end date
	 * @param  Date  $data_end start date
	 * @param int    $id       dealer or site ID, depending on how the
	 *                         dealer scores should be aggregated
	 * @param string $id_type  'dealerID' or 'siteID' type of data
	 *                         aggrtegation
	 * 
	 * @return array dictionary of scores in the form:
	 *   |- [site ID]
	 *   .   |- [item name]
	 *   .   .    |- ['date'] => score
	 *   .   .    .
	 *   .   .    .
	 *   .   .    |- nth score
	 *   .   |- [nth item name]
	 *   |- [nth site ID]
	 */
	public function fetchAllGradesForRange(
		Date $data_end,
		Date $data_start,
		int $id,
		string $id_type
	) : array {
		// Use connection and id / id type to get data and organize
		
		return $organized_data;
	}
	
	public function fetchTestimonialStats()
	{
		// Get raw testimonial stats
	}
	
	/*
		other specific inventory fetch methods go here
	 */
	
	public function saveGrade($date, $grade_type, $score) {
		
	}
}
