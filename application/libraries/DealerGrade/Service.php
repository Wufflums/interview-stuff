<?php
namespace APP\DealerGrade;

/**
 * Dealer Grade service.
 */
class Service
{
	/**
	 * @var Factory Dealer Grade data object factory
	 */
	private $factory;
	/**
	 * @var Provider Dealer Grade data provider
	 */
	private $provider;
	
	/**
	 * Constructor for Dealer Grade service.
	 * 
	 * @param Factory  $factory  Dealer Grade data object factory
	 * @param Provider $provider Dealer Grade data provider
	 */
	public function __construct(Factory $factory, Provider $provide, int $id, string $id_type)
	{
		$this->factory = $factory;
		$this->service = $service;
	}
	
	/**
	 * Gets the average score for a site or dealer.
	 * 
	 * @param  Date $start start date
	 * @param  Date $end   end date
	 * 
	 * @return int average site / dealer score
	 * 
	 * @throws InsufficientViewDataException when not enough data to create
	 *         average
	 */
	public function getAverageTotalGrade(Date $start, Date $end) : int
	{
		$grade_data = $this->provider->fetchAllGradesForRange($start, $end);
		
		if (empty($grade_data)) {
			throw \Exception\InsufficientDataException('Not enough data to create average.');
		}
		
		// Do averaging stuff
		// $grade_data >> $prepared_data
		
		$view_data = $this->factory->buildAverageTotalGradeContent(
			$prepared_data
		);
		
		return $view_data;
	}
	
	/**
	 * Gets the data object collection for scorable items.
	 * 
	 * @param  Date   $start start date
	 * @param  Date   $end   end date
	 * 
	 * @return GradeCollection collection of score items
	 * 
	 * @throws InsufficientViewDataException when not enough data to create
	 *         averages
	 */
	public function getGradeItemAverageCollection(Date $start, Date $end) : GradeCollection
	{
		$grade_data = $this->provider->fetchAllGradesForRange($start, $end);
		
		if (empty($grade_data)) {
			throw \Exception\InsufficientDataException('Not enough data to create averages.');
		}
		
		// Do averaging stuff for each item
		// $grade_data >> $prepared_data
		
		/*
			Output structure:
			'location_segment' => site / dealer page link 
			'items' => [
				[
					'score' => the int score
					'item' => the item string that the score is for
					'inventory_segment' => the inventory link last segment for this item type
				],
				...
			]
		 */
		
		$view_data = $this->factory->buildAverageItemGradesCollection(
			$prepared_data
		);
		
		return $view_data;
	}
	
	/**
	 * Gets the average grades for each day in a date range.
	 * 
	 * @param  Date   $start start date
	 * @param  Date   $end   end date
	 * 
	 * @return GradeCollection collection of GradeItemContent structs
	 * 
	 * @throws InsufficientViewDataException when not enough data to create
	 *         averages
	 */
	public function getAverageTotalGrades(Date $start, Date $end)
	{
		$grade_data = $this->provider->fetchAllGradesForRange($start, $end);
		
		if (empty($grade_data)) {
			throw \Exception\InsufficientDataException('Not enough data to create averages.');
		}
		
		// Do averaging stuff for each date between start and end
		// $grade_data >> $prepared_data
		
		$view_data = $this->factory->buildAverageTotalGradesCollection(
			$prepared_data
		);
		
		return $view_data;
	}
	
	/**
	 * Gets item grades for the date given.
	 * 
	 * @param Date $date date to search by
	 * 
	 * @return GradeCollection collection of grade objects
	 * 
	 * @throws InsufficientViewDataException when no item data
	 */
	public function getItemGrades(Date $date) : GradeCollection
	{
		$grade_data = $this->provider->fetchAllGradesForRange($date, $date);
		
		if (empty($grade_data)) {
			throw \Exception\InsufficientDataException('No available item data.');
		}
		
		// Average (if there is more than one site)
		// $grade_data >> $prepared_data
		
		/*
		
			Output structure:
			[
				[
					'score' => int score
					'item' => item string name
				]
			]
		 */
		
		$view_data = $this->factory->buildItemsGradesCollection($prepared_data);
		
		return $view_data;
	}
}
