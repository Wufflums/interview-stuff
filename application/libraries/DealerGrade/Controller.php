<?php
namespace APP\DealerGrade;

/**
 * Dealer Grade controller for managing the service.
 */
class Controller
{
	/**
	 * @var Service Dealer Grade service
	 */
	private $service;
	/**
	 * @var ViewBuilder Dealer Grade view builder
	 */
	private $view_builder;
	
	/**
	 * Constructor for Dealer Grade service controller.
	 * 
	 * @param Service     $service      Dealer Grade service
	 * @param ViewBuilder $view_builder Dealer Grade view builder
	 */
	public function __construct(Service $service, ViewBuilder $view_builder)
	{
		$this->service = $service;
		$this->view_builder = $view_builder;
	}
	
	/**
	 * Builds the average site / dealer score over some time period box view.
	 * 
	 * @uses Service::getGradeAverage() to get average grade data for a given
	 *       date range
	 * @uses ViewBuilder::buildGradeAverageView() to build average grade box
	 *       view
	 * 
	 * @return View\GradeAverageView average site / dealer Dealer Grade view
	 */
	public function getGradeAverageView(
		Date $date_end,
		Date $date_start,
		int $id,
		string $id_type,
		bool $minify_flag = true
	) : View\GradeAverageView {
		$view_data = $this->service->getAverageTotalGrade(
			$date_end,
			$date_start,
			$id,
			$id_type
		);
		
		$view = $this->view_builder->buildGradeAverageView(
			$view_data,
			$minify_flag
		);
		
		return $view;
	}
	
	/**
	 * Builds the grade breakdown box featuring all the graded items over some
	 * time period.
	 * 
	 * @uses Service::getAvergageGradeItemCollection() to get item collection
	 *       of all average scores of each graded item
	 * @uses ViewBuilder::buildGradeBreakdownView() to build average grade per
	 *       category breakdown view
	 * 
	 * @return View\GradeBreakdownView grade breakdown for each graded item
	 */
	public function getGradeBreakdownView(
		Date $date_end,
		Date $date_start,
		int $id,
		string $id_type,
		bool $minify_flag = true
	) : View\GradeBreakdownView {
		$grade_collection = $this->service->getAvergageGradeItemCollection();
		
		$view = $this->view_builder->buildGradeBreakdownView();
		
		return $view;
	}
	
	/**
	 * Builds the grade chart with all the daily average scores over some time
	 * period.
	 * 
	 * @uses Service::getAverageGradeCollection() to get collection of average
	 *       scores over given time period
	 * @uses ViewBuilder::buildGradeChartView() to build Highcharts average
	 *       grades over time view
	 * 
	 * @param Date   $date_end    data end date
	 * @param Date   $date_start  data start date
	 * @param int    $id          site / dealer ID
	 * @param string $id_type     ID type (site or dealer)
	 * @param bool   $minify_flag CSS minification flag
	 * 
	 * @return View\GradeChartView Highcharts average grade chart
	 */
	public function getGradeChartView(
		Date $date_end,
		Date $date_start,
		int $id,
		string $id_type,
		bool $minify_flag = true
	) : View\GradeChartView {
		$view_data = $this->service->getAverageTotalGrades(
			$date_end,
			$date_start,
			$id,
			$id_type
		);
		
		$view = $this->view_builder->buildGradeChartView(
			$view_data,
			$minify_flag
		);
		
		return $view;
	}
	
	/**
	 * Builds the list of category scores for a specific date.
	 * 
	 * @param Date   $date        date to get data
	 * @param int    $id          site / dealer ID
	 * @param string $id_type     ID type (site or dealer)
	 * @param bool   $minify_flag CSS minification flag
	 * 
	 * @return \View\GradeItemsView list of grades for all graded items view
	 */
	public function getGradeItemsView(
		Date $date,
		int $id,
		string $id_type,
		bool $minify_flag = true
	) : View\GradeItemsParentView {
		$view_data = $this->service->getItemGrades($date, $id, $id_type);
		
		$view = $this->view_builder->buildGradeItemsParentView(
			$view_data,
			$minify_flag
		);
		
		return $view;
	}
}
