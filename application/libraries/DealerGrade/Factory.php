<?php
namespace APP\DealerGrade;

/**
 * Dealer Grade service data object factory.
 */
class Factory
{
	public function buildAverageTotalGradeContent(array $data)
	{
		// Make view stuff
		
		return new Data\GradeItemContent(/* Stuff */);
	}
	
	public function buildAverageTotalGradesCollection(array $data)
	{
		// Build each GradeItemContent and add to GradeCollection
		
		return new Data\GradeCollection ($content);
	}
	
	public function buildAverageItemGradesCollection(array $data)
	{
		// Build each GradeItemContent and add to GradeCollection
		
		return new Data\GradeCollection ($content);
	}
	
	public function buildItemsGradesCollection(array $data)
	{
		// Build each GradeItemView and add to Grade Collection
		
		return new Data\GradeCollection ($content);
	}
}
