<?php
include_once($GLOBALS['application_folder'] . '/modules/parents/grandparent.php');

/**
 * CI controller for outputting the Dealer Grade graph module using Highscharts.
 *
 * Renamed module "Graph_Score" to be more intuitive.
 */
class GradeChart extends GrandParent
{
	protected $date_start;
	protected $date_end;
	
	/**
	 * Constructor for Dealer Grade chart CI controller.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->model->load('UserModel');
	}
	
	/**
	 * Method fired automatically on page load as part of CodeIgniter.
	 */
	public function _index_process()
	{
		$this->view_file = null;
		
		try {
			$view = $this->buildDealerGradeChart();
			
			$this->V['view_class'] = $view;
		} catch (\InsufficientViewDataException $e) {
			// Dealer Grade is not on for this site.
			return;
		} catch (\Throwable $e) {
			trigger_error('@Viewerror '.get_class($this).' '.$e->getMessage());
		}
	}
}
