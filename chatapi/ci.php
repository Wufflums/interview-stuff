<?php

class Department extends MX_Controller
{
	public function __construct()
	{
		// Validate URL + query
		$this->validateURL();
		
		// Authenticate user
		$authorized = $this->authenticateUser();
		
		if (!$authorized) {
			$this->createError();
		}
		
		if ($seg === 'docs') {
			$this->load->view('private/resrc/department/documentation.php');
		}
		
		// Get data
		$this->request();
	}
	
	private function request()
	{
		$service = $this->getDepartmentService();
		
		try {
			$output = $service->getData();
		} catch (\Throwable $e) {
			$output = $this->createError(get_class($e));
		}
		
		if ($this->debug === true) {
			$output = $this->debugOutput($output);
		}
		
		echo $output;
	}
}
