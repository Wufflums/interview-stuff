<?php
namespace DEP\Phoenix\Request;

interface ProviderInterface
{
	public function fetchBySegment(string $segments, array $filters);
}
