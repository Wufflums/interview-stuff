<?php
namespace DEP\Phoenix\Request;

use DEP\Phoenix\Application\MysqliExtended;

class ServiceBuilder
{
	public function buildService(MysqliExtended &$db, bool $filtered, int $site_id) : Service
	{
		if ($filtered) {
			$provider = $this->buildDepartmentFilteredProvider($db, $site_id);
		} else {
			$provider = $this->buildDepartmentProvider($db, $site_id);
		}
		
		return new Service($provider);
	}
	
	private function buildDepartmentProvider(
		MysqliExtended &$db,
		int $site_id
	) : DepartmentProviderInterface
	{
		return new DepartmentProvider($db, $site_id);
	}
	
	private function buildDepartmentFilteredProvider(
		MysqliExtended &$db,
		int $site_id
	) : DepartmentProviderInterface
	{
		return new DepartmentFilteredProvider($db, $side_id);
	}
}
