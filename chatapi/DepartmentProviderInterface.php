<?php
namespace DEP\Phoenix\Request;

interface DepartmentProviderInterface
{
	public function fetchDepartment(array $filters) : array;
	public function fetchSpecialHours(array $filters) : array;
	public function fetchStandardHours(array $filters) : array;
	public function fetchStaff(array $filters) : array;
}