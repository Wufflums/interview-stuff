<?php
namespace DEP\Phoenix\Request;

class Service
{
	public function __construct(ProviderInterface $provider)
	{
		$this->provider = $provider;
	}
	
	public function getData(string $segment, array $filters) {
		$data = $this->provider->fetchBySegment($segment, $filters);
		
		// Do data stuff
		
		return $data;
	}
}
