<?php
namespace DEP\Phoenix\Request;

class DepartmentProvider implements DepartmentProviderInterface, ProviderInterface
{
	public function __construct(MysqliExtended &$db, int $site_id)
	{
		$this->setConnection($db);
		$this->setSiteID($site_id);
	}
	
	public function fetchBySegment(string $segment, array $filters) : array {}
	
	public function fetchDepartment(array $filters) : array {}
	public function fetchSpecialHours(array $filters) : array {}
	public function fetchStaff(array $filters) : array {}
	public function fetchStandardHours(array $filters) : array {}
	
	private function getConnection() : MysqliExtended {}
	private function getSiteID() : int {}
	private function setConnection(MysqliExtended &$db) {}
	private function setSiteID(int $site_id) {}
}
